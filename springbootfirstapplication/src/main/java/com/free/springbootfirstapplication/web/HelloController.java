package com.free.springbootfirstapplication.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: SpringBootDemo
 * @description:
 * @author: Mr.Zhou
 * @create: 2019-09-24 23:2019:05
 **/
@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {

        return "hello Spring Boot !";
    }
}
